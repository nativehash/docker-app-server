## Install

> Is important tu set correct users in .env file before run ```./install``` script.

First create preconfigured .env file:

```
./preset-env {project_domain} {app_git_clone_link}
```

Where {project_domain} is like: ```mydomain.com``` and {app_git_clone_link} include access token like: ```https://user:access_key@gitlab.com/nativehash/vueapp.git```.


Check ```.env``` file and if is correct run:

```
./install
```

## Run

```
sudo docker-compose up
```

For standalone - not for webproxy - version run:

```
sudo docker-compose -f ./docker-compose-standalone.yml up
```

Stop:

```
sudo docker-compose down
```

## Tools

### update/compile app

```sh
./app-update
```

### Autostart

If script path is other than the default, change before copy.

```
sudo cp mdcms-server.service /etc/systemd/system/
sudo systemctl start mdcms-server && sudo systemctl enable mdcms-server
```
